package firstpracticalproject.model;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Task {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "taskId")
    private int taskId;
@Column(name = "description")
    private String description;

    public Task() {}

    public String getDescription() {
        return description;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "Task(id = " + taskId + ")" + ": " + description;
    }
}
