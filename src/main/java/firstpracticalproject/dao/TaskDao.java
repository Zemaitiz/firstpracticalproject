package firstpracticalproject.dao;

import firstpracticalproject.model.Task;
import firstpracticalproject.services.SessionManager;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class TaskDao {


    //Create
    public void createTask(Task task) {
        Transaction transaction = null;
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(task);
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    // Read
    public Task readById(int taskId) {
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            Task task = session.find(Task.class, taskId);
            return task;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //READ ALL

    public List<Task> getAllTasks() {
        try {
            Session session = SessionManager.getSessionFactory().openSession();

            return session.createQuery("from Task").list();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //UPDATE
    public void updateDescById(int taskId, String description) {
        Transaction transaction = null;

        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Task task = session.find(Task.class, taskId);
            task.setDescription(description);
            session.save(task);
            transaction.commit();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //DELETE
    public void deleteById(int taskId) {
        Transaction transaction = null;

        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Task task = session.find(Task.class, taskId);
            session.delete(task);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //DELETE ALL tasks

    public void deleteAll() {
        Transaction transaction = null;

        try {
            Session session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            List<Task> taskList = getAllTasks();
            for (Task task : taskList) {
                deleteById(task.getTaskId());
            }
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     /* List<Task> taskList = taskDao.getAllTasks();
        for (Task t : taskList) {
            taskDao.deleteById(t.getTaskId());
        }*/


}
