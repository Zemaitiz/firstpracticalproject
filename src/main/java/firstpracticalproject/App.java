package firstpracticalproject;

import firstpracticalproject.dao.TaskDao;
import firstpracticalproject.model.Task;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicReference;

public class App extends Application {


    public static void main(String[] args) {

        App.launch();


    }

    @Override
    public void start(Stage primaryStage) {
        TaskDao taskDao = new TaskDao();

        //Button layout
        HBox buttonHBox = new HBox();
        ObservableList<Node> buttons = buttonHBox.getChildren();

        //Id and description fields layout

        HBox textFieldsHBox = new HBox();
        ObservableList<Node> textFields = textFieldsHBox.getChildren();

        //Result view

        Label result = new Label("There are no tasks here yet");

        //Master Layout
        VBox layout = new VBox(buttonHBox, textFieldsHBox, result);


        //Create a task

        Button createTask = new Button("Create a new task");
        buttons.add(createTask);
        TextField taskDesc = new TextField("Enter task description here and then press create a new task button");
        textFields.add(taskDesc);

        createTask.setOnAction(event -> {
                    Task task = new Task();
                    task.setDescription(taskDesc.getText());
                    taskDao.createTask(task);
                }
        );

        //Read a task by ID

        Button readById = new Button("Find by id");
        TextField taskId = new TextField("Enter task id");
        buttons.add(readById);
        textFields.add(taskId);


        readById.setOnAction(event -> result.setText(taskDao.readById(Integer.parseInt(taskId.getText())).toString()));


        //Update a task by id _ NOT finished


        TextField newDesc = new TextField("Enter new task description here and press update task button");
        Button updateTask = new Button("Update task description");
        buttons.add(updateTask);
        textFields.add(newDesc);

        updateTask.setOnAction(event -> taskDao.updateDescById(Integer.parseInt(taskId.getText()), newDesc.getText()));


        //Delete all button

        Button deleteAllTasks = new Button("Delete all tasks");
        buttons.add(deleteAllTasks);
        deleteAllTasks.setOnAction(event -> taskDao.deleteAll());


        Scene scene = new Scene(layout);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
